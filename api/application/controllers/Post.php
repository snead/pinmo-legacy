<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	/*
	 * 
	 * ArtAlbum Content API Controller
	 *
	 * @author: Francisco Rader
	 * @email: franciscorader@gmail.com
	 * @project: ArtAlbum
	 * @git: https://gitlab.com/snead/artalbum.git
	 * @license: GPL
	 *  
	 */

	function __construct()
    {
        parent::__construct();
    }

    function album($id = false)
    {
    	try {

    		$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);
			$token = $request->token;

    		if ($this->session->userdata('token') !== $token) {

				$this->output->set_status_header(401)
							 ->set_header('HTTP/1.1 401 INVALID TOKEN');
			} else {			

	    		$this->load->model('Album');
				if($id) {
					$query = $this->Album->updateAlbum($id, $request);
				} else {
					$query = $this->Album->insertAlbum($request);
				}
		    	
		    	if(@$query) {
		    		$this->output
						 ->set_status_header(200)
						 ->set_header('HTTP/1.1 200 OK');
		    	}
			}
    		


	    }
	    catch(Exception $e)
		{
			$this->output
			->set_status_header(415)
			->set_header('HTTP/1.1 415 '.$e);
		}
    }

    function image() {
    	try {

    		if ($this->session->userdata('token') !== $this->input->post('token')) {
    			throw new Exception('Invalid token');
    		}

    		$config['upload_path'] = '../uploaded/images/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '67584';
			$config['max_width']  = '5000';
			$config['max_height']  = '5000';
			$config['file_ext_tolower'] = true;

			$this->load->model('Album');

			$album_id = $this->input->post('album_id');
			$album_title = $this->Album->getAlbumTitle($album_id);
			$album_url = strtolower(url_title($album_title));

			$explode = explode('.', $_FILES['file']['name']);

			$extension = strtolower($explode[(count($explode) - 1)]);

			$oldname = strtolower(url_title($explode[0]));

			$newname = $album_url.'-'.$oldname.'.'.$extension;

			while (
				file_exists($config['upload_path'] . $newname)
				|| strlen($newname) >= 255
			) {
				$newname = $album_url.'-IMG'.rand(0,9999).'.'.$extension;
			}

			$config['file_name'] = $newname;

			$this->load->library('upload', $config);
    		if ( ! $this->upload->do_upload('file'))
			{
				throw new Exception($this->upload->errors());
			}
			else
			{
				$insert = array(
					'parent_id' => $album_id,
					'parent_title' => $album_title,
					'date' => date('d-m-Y', time()),
					'type' => 'images',
					'title' => $oldname,
					'text' => '',
					'created' => time(), 
					'updated' => time(),
					'filename' => $newname
				);

				if ($this->db->insert('files', $insert)) {
				echo "Succesfull";
				$this->output
					 ->set_status_header(200)
					 ->set_header('HTTP/1.1 200 OK');

				} else {
					throw new Exception('Error inserting into DB');					
				}
			}
	    }
	    catch(Exception $e)
		{
			$this->output
					 ->set_status_header(415)
					 ->set_header('HTTP/1.1 200 '.$e);
		}
    }
}