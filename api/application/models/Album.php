<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
    function insertAlbum( $data ) {

        $insert = array(
            'title' => @$data->title,
            'date' => @$data->date,
            'title_url' => strtolower(url_title($data->title))
        );

        if($data->date == null) {
            $insert['date'] = ' ';
        }
         if($data->text == null) {
            $insert['text'] = ' ';
        }

        if(isset($data->title)) {
            if ($this->db->insert('albums', $insert)) {
                return true;
            } else {
                throw new Exception('Error inserting into db');
            }
        } else {
            throw new Exception('Title is empty');
        }
       
    }

    function updateAlbum( $id , $data ) {

        $insert = array(
            'title' => @$data->title,
            'date' => @$data->date,
            'text' => @$data->text
        );

        $this->db->where('album_id', $id);

        if($this->db->update('albums', $insert)) {
            return true;
        } else {
            throw new Exception('Error updating album');
        }
    }

	/*
     * getAlbumList get the user object by email
     * @param 'string' 'email'
     */
    function getAlbumList() {

        $albums = $this->db->order_by('album_id', 'desc');
        $albums = $this->db
            ->select("album_id,title,date,title_url")
            ->get("albums")
            ->result_array();

        if (empty($albums)) {
            throw new Exception('No albums found');
        }

        return $albums;
    }

    function getAlbumTitle( $id ) {
        if ($id) {
            $album = $this->db->get_where('albums', array('album_id' => $id));
            $album = $album->row();
            return $album->title;
        } else {
            throw new Exception('Album doesn\'t exist');            
        }
    }

    function getImagesList($album = false) {

        if ($album) {
            $images = $this->db
                ->where(
                    array(
                        'parent_id' => $album,
                        'type' => 'images'
                    )
                );
        } else {
            $images = $this->db
                ->where(
                    array(
                        'type' => 'images'
                    )
                );
        }

        $images = $this->db->order_by('file_id', 'desc');
        $images = $images->get('files')->result_array();

        if (empty($images)) {
            throw new Exception('No images found');
        }

        return $images;
    }


}