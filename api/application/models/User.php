<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

    var $user_id	= '';
    var $name		= '';
    var $email		= '';
    var $lastname	= '';
    var $created	= '';
    var $updated	= '';
    var $password	= '';

    function __construct()
    {
        parent::__construct();
    }
	
	/*
     * @getUser get the user object by email
     * @param 'string' 'email'
     */
    function getUser( $email ) {

    	$user = $this->db
    		->get_where('users', array('email'=>strip_tags($email)))
    		->row();
    	
        if($user === null) {
            $user = false;
        }

    	return $user;
    }

}